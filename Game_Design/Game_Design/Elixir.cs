﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Game_Design
{
    class Elixir
    {
        //This is for the direction which the elixir will shoot
        public string direction;
        public int elixirLeft;
        public int elixirTop;

        //This is for the speed which the elixir will shoot at
        private int speed = 15;
        private PictureBox elixir = new PictureBox();
        private Timer elixirTimer = new Timer();

        public void MakeElixir(Form form)
        {
            //This is the function to add the elixir to the game
            elixir.Image = Properties.Resources.Elixir;
            elixir.SizeMode = PictureBoxSizeMode.AutoSize;
            elixir.Tag = "elixir";
            elixir.Left = elixirLeft;
            elixir.Top = elixirTop;
            elixir.BringToFront();

            form.Controls.Add(elixir);

            elixirTimer.Interval = speed;
            elixirTimer.Tick += new EventHandler(ElixirTimerEvent);
            elixirTimer.Start();

        }

        private void ElixirTimerEvent(object sender, EventArgs e)
        {
            //This is for the direction which the elixir will face
            if (direction == "up")
            {
                elixir.Top -= speed;
            }
            if (direction == "down")
            {
                elixir.Top += speed;
            }
            if (direction == "left")
            {
                elixir.Left -= speed;
            }
            if (direction == "right")
            {
                elixir.Left += speed;
            }

            //This is for when the elixir is near the bounds on the game and will dispose of the object
            if (elixir.Left < 5 || elixir.Left > 935 || elixir.Top < 5 || elixir.Top > 695)
            {
                elixirTimer.Stop();
                elixirTimer.Dispose();
                elixir.Dispose();
                elixirTimer = null;
                elixir = null;
            }
        }

    }
}
