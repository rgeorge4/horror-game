﻿namespace Game_Design
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtBatteries = new System.Windows.Forms.Label();
            this.txtKills = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.HealthBar = new System.Windows.Forms.ProgressBar();
            this.GameTimer = new System.Windows.Forms.Timer(this.components);
            this.player = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.player)).BeginInit();
            this.SuspendLayout();
            // 
            // txtBatteries
            // 
            this.txtBatteries.AutoSize = true;
            this.txtBatteries.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBatteries.Location = new System.Drawing.Point(12, 12);
            this.txtBatteries.Name = "txtBatteries";
            this.txtBatteries.Size = new System.Drawing.Size(200, 37);
            this.txtBatteries.TabIndex = 0;
            this.txtBatteries.Text = "Batteries : 0";
            // 
            // txtKills
            // 
            this.txtKills.AutoSize = true;
            this.txtKills.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKills.Location = new System.Drawing.Point(763, 12);
            this.txtKills.Name = "txtKills";
            this.txtKills.Size = new System.Drawing.Size(129, 37);
            this.txtKills.TabIndex = 1;
            this.txtKills.Text = "Kills : 0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(254, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(135, 37);
            this.label3.TabIndex = 2;
            this.label3.Text = "Health :";
            // 
            // HealthBar
            // 
            this.HealthBar.Location = new System.Drawing.Point(395, 12);
            this.HealthBar.Name = "HealthBar";
            this.HealthBar.Size = new System.Drawing.Size(326, 37);
            this.HealthBar.TabIndex = 3;
            this.HealthBar.Value = 100;
            // 
            // GameTimer
            // 
            this.GameTimer.Enabled = true;
            this.GameTimer.Interval = 20;
            this.GameTimer.Tick += new System.EventHandler(this.MainTimer);
            // 
            // player
            // 
            this.player.Image = global::Game_Design.Properties.Resources.Jup;
            this.player.Location = new System.Drawing.Point(414, 496);
            this.player.Name = "player";
            this.player.Size = new System.Drawing.Size(45, 63);
            this.player.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.player.TabIndex = 4;
            this.player.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Bisque;
            this.ClientSize = new System.Drawing.Size(918, 644);
            this.Controls.Add(this.player);
            this.Controls.Add(this.HealthBar);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtKills);
            this.Controls.Add(this.txtBatteries);
            this.Name = "Form1";
            this.Text = "Corn maze";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TheKeyIsDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TheKeyIsUp);
            ((System.ComponentModel.ISupportInitialize)(this.player)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label txtBatteries;
        private System.Windows.Forms.Label txtKills;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ProgressBar HealthBar;
        private System.Windows.Forms.Timer GameTimer;
        private System.Windows.Forms.PictureBox player;
    }
}

