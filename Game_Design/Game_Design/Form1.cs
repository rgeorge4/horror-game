﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Game_Design
{
    public partial class Form1 : Form
    {
        //Variables

        bool moveUp, moveDown, moveLeft, moveRight; //This is for the player when moving around the screen
        bool End; //This is to end the game
        string facing = "up"; //This string is for guiding the elixir
        int playerHealth = 100; //This is for player health
        int speed = 10; // This is for the speed of the player
        int batteries = 5; //This is for the set amount of "ammo" the player will have from the start
        int scarecrowSpeed = 3; //This is the speed of the scarecrow enemy
        int score; //This is for the score/kills the player gets throughout the game
        Random randnum = new Random(); //This is a random class to make a number

        List<PictureBox> scarecrowList = new List<PictureBox>();

        public Form1()
        {
            InitializeComponent();
            Restart();
        }

        private void MainTimer(object sender, EventArgs e)
        {
            //This is for when the player reaches 1 health and dies 
            if (playerHealth > 1)
            {
                HealthBar.Value = playerHealth;
            }
            else
            {
                End = true;
                player.Image = Properties.Resources.Dead;
                GameTimer.Stop();
            }

            //This is to show the amount of batteries left and the amount of kills the player has
            txtBatteries.Text = "Batteries: " + batteries;
            txtKills.Text = "Kills: " + score;

            // This is to allow the player to move upwards if moveUp is true 
            if (moveUp == true && player.Top > 45)
            {
                player.Top -= speed;
            }
            // This is to allow the player to move downwards if moveDown is true 
            if (moveDown == true && player.Top + player.Height < this.ClientSize.Height)
            {
                player.Top += speed;
            }
            // This is to allow the player to move lrft if moveLeft is true 
            if (moveLeft == true && player.Left > 0)
            {
                player.Left -= speed;
            }
            // This is to allow the player to move right if moveRight is true 
            if (moveRight == true && player.Left + player.Width < this.ClientSize.Width)
            {
                player.Left += speed;
            }

            //This is for if the player runs over the pickup and collects it
            foreach (Control x in this.Controls)
            {
                if (x is PictureBox && (string)x.Tag == "batteries")
                {
                    if (player.Bounds.IntersectsWith(x.Bounds))
                    {
                        this.Controls.Remove(x);
                        ((PictureBox)x).Dispose();
                        batteries += 5;
                    }

                }

                //This is for when the player gets hit by a scarecrow
                if (x is PictureBox && (string)x.Tag == "scarecrow")
                {

                    //This is for the way the scarecrow will face when towards the player
                    if(x.Left > player.Left)
                    {
                        x.Left -= scarecrowSpeed;
                        ((PictureBox)x).Image = Properties.Resources.Sleft;
                    }
                    if (x.Left < player.Left)
                    {
                        x.Left += scarecrowSpeed;
                        ((PictureBox)x).Image = Properties.Resources.Sright;
                    }
                    if (x.Top > player.Top)
                    {
                        x.Top -= scarecrowSpeed;
                        ((PictureBox)x).Image = Properties.Resources.Sdown;
                    }
                    if (x.Top < player.Top)
                    {
                        x.Top += scarecrowSpeed;
                        ((PictureBox)x).Image = Properties.Resources.Sup;
                    }
                    //This is for when the player is attacked by the scarecrows and loses health
                    if (player.Bounds.IntersectsWith(x.Bounds))
                    {
                        playerHealth -= 1;
                    }
                    if (End == true)
                    {
                        return;
                    }

                }

                
                foreach (Control h in this.Controls)
                {
                    //This is to identify the elixir and scarecrows
                    if (h is PictureBox && (string)h.Tag == "elixir" && x is PictureBox && (string)x.Tag == "scarecrow")
                    {
                        //This is for increasing the score
                        if (x.Bounds.IntersectsWith(h.Bounds))
                        {
                            score++;

                            //This is for removal of the elixir and scarecrows from the screen
                            this.Controls.Remove(h);
                            ((PictureBox)h).Dispose();
                            this.Controls.Remove(x);
                            ((PictureBox)x).Dispose();
                            scarecrowList.Remove(((PictureBox)x));
                            //This is to spawn in more scarecrows
                            SpawnScarecrows();
                        }

                    }

            }

            }
            
            


        }

        private void TheKeyIsDown(object sender, KeyEventArgs e)
        {
            //This end is true nothing will happen
            if(End == true)
            {
                return;
            }

            //This is for the player to change direction to go up
            if (e.KeyCode == Keys.Up)
            {
                moveUp = true;
                facing = "up";
                player.Image = Properties.Resources.Jup;
            }

            //This is for the player to change direction to go down
            if (e.KeyCode == Keys.Down)
            {
                moveDown = true;
                facing = "down";
                player.Image = Properties.Resources.Jdown;
            }

            //This is for the player to change direction to go left
            if (e.KeyCode == Keys.Left)
            {
                moveLeft = true;
                facing = "left";
                player.Image = Properties.Resources.Jleft;
            }

            //This is for the player to change direction to go right
            if (e.KeyCode == Keys.Right)
            {
                moveRight = true;
                facing = "right";
                player.Image = Properties.Resources.Jright;
            }
        }


        private void TheKeyIsUp(object sender, KeyEventArgs e)
        {
            //This is for when the up key is lifted and stops the player from moving that direction
            if (e.KeyCode == Keys.Up)
            {
                moveUp = false;
            }

            //This is for when the down key is lifted and stops the player from moving that direction
            if (e.KeyCode == Keys.Down)
            {
                moveDown = false;
            }

            //This is for when the left key is lifted and stops the player from moving that direction
            if (e.KeyCode == Keys.Left)
            {
                moveLeft = false;
            }

            //This is for when the right key is lifted and stops the player from moving that direction
            if (e.KeyCode == Keys.Right)
            {
                moveRight = false;
            }

            //This is for the "ammo" function to allow the gun to shoot when space key is released
            if (e.KeyCode == Keys.Space && batteries > 0 && End == false)

            {
                
                batteries--; //This is for reducing the ammount of batteries the player has left 
                LaunchElixir(facing); //This is for the direction which the elixir will shoot

                //This is for when the batteries are less than 1, a pack will spawn on the map
                if (batteries < 1)
                {
                    SpawnBatteries();
                }
            }

            //This is for resetting the game once the button R has been released
            if (e.KeyCode == Keys.R && End == true)
            {
                Restart();
            }


        }

        private void LaunchElixir(string direction)
        {
            //this is the function to make new elixirs
            Elixir launchElixir = new Elixir();
            launchElixir.direction = direction;
            launchElixir.elixirLeft = player.Left + (player.Width / 2);
            launchElixir.elixirTop = player.Top + (player.Height / 2);
            launchElixir.MakeElixir(this);
        }

        private void SpawnBatteries()
        {
            //This is the function to spawn in batteries
            PictureBox batteries = new PictureBox();
            batteries.Tag = "batteries";
            batteries.Image = Properties.Resources.Battery;
            batteries.SizeMode = PictureBoxSizeMode.AutoSize;
            batteries.Top = randnum.Next(10, this.ClientSize.Height - batteries.Height);
            batteries.Left = randnum.Next(10, this.ClientSize.Width - batteries.Width);
            this.Controls.Add(batteries);

            batteries.BringToFront();
            player.BringToFront();
            
        }

        private void SpawnScarecrows()
        {
            //Thsi si the function to spawn in scarecrows 
            PictureBox scarecrow = new PictureBox();
            scarecrow.Tag = "scarecrow";
            scarecrow.Image = Properties.Resources.Sdown;
            scarecrow.Top = randnum.Next(0, 800);
            scarecrow.Left = randnum.Next(0, 900);
            scarecrow.SizeMode = PictureBoxSizeMode.AutoSize;
            scarecrowList.Add(scarecrow);
            this.Controls.Add(scarecrow);
            player.BringToFront();

        }

        private void Restart()
        {
            //This is the function to restart the game once the player has died
            foreach (PictureBox s in scarecrowList)
            {
                this.Controls.Remove(s);
            }
            scarecrowList.Clear();

            for (int s = 0; s < 3; s++)
            {
                SpawnScarecrows();
            }

            moveUp = false;
            moveDown = false;
            moveLeft = false;
            moveRight = false;
            End = false;

            playerHealth = 100;
            score = 0;
            batteries = 5;

                GameTimer.Start();
        }
    }
}
